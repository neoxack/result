﻿namespace Neoxack.Result;

public readonly struct Result<T>
{
    public bool Success { get; }
    public bool Failure => !Success;
    private readonly string? _error;
    private readonly T? _value;
    public T Value
    {
        get
        {
            if (!Success)
            {
                throw new InvalidOperationException(Errors.DataIsEmptyInFailedResult);
            }

            return _value!;
        }
    }
    public string Error
    {
        get
        {
            if (Success)
            {
                throw new InvalidOperationException(Errors.ErrorIsEmptyInSuccessResult);
            }

            return _error!;
        }
    }

    public Result(T value)
    {
        _value = value;
        Success = true;
        _error = null;
    }

    public Result(bool isSuccess, string? error)
    {
        Success = isSuccess;
        _error = error;
    }

    public static implicit operator Result<T>(T value) => new Result<T>(value);
    public static implicit operator Result<T>(Result result) => new Result<T>(false, result.Error);
}

public readonly struct Result
{
    public bool Success { get; }
    public bool Failure => !Success;
    private readonly string? _error;
    
    public string Error
    {
        get
        {
            if (Success)
            {
                throw new InvalidOperationException(Errors.ErrorIsEmptyInSuccessResult);
            }

            return _error!;
        }
    }

    private Result(bool success, string? error = null)
    {
        Success = success;
        _error = error;
    }

    public static Result Ok()
    {
        return new Result(true);
    }

    public static Result Fail(string error)
    {
        return new Result(false, error);
    }

    public static Result<T> Ok<T>(T value)
    {
        return new Result<T>(value);
    }

    public static Result<T> Fail<T>(string error)
    {
        return new Result<T>(false, error);
    }
}

file static class Errors
{
    public const string DataIsEmptyInFailedResult = "Value is empty in failed result";
    public const string ErrorIsEmptyInSuccessResult = "Error is empty in success result";
}